package com.xiaoyueyin.decorate.service;
import com.xiaoyueyin.decorate.model.User;
import com.xiaoyueyin.decorate.core.Service;


/**
 * Created by CodeGenerator on 2018/04/30.
 */
public interface UserService extends Service<User> {
    /**
     * 查询用户
     *
     * @param username 用户名
     * @param pwd      密码
     * @return 用户信息 user info
     * @throws Exception the exception
     */
    public User checkPwd(String username, String pwd)
            throws Exception;
}
