package com.xiaoyueyin.decorate.service.impl;

import com.xiaoyueyin.decorate.dao.ContractMapper;
import com.xiaoyueyin.decorate.model.Contract;
import com.xiaoyueyin.decorate.service.ContractService;
import com.xiaoyueyin.decorate.core.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by CodeGenerator on 2018/04/30.
 */
@Service
@Transactional
public class ContractServiceImpl extends AbstractService<Contract> implements ContractService {
    @Resource
    private ContractMapper contractMapper;

}
