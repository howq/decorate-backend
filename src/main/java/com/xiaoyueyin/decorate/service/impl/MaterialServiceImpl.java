package com.xiaoyueyin.decorate.service.impl;

import com.xiaoyueyin.decorate.dao.MaterialMapper;
import com.xiaoyueyin.decorate.model.Material;
import com.xiaoyueyin.decorate.service.MaterialService;
import com.xiaoyueyin.decorate.core.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;


/**
 * Created by CodeGenerator on 2018/04/30.
 */
@Service
@Transactional
public class MaterialServiceImpl extends AbstractService<Material> implements MaterialService {
    @Resource
    private MaterialMapper materialMapper;

    @Override
    public List<Material> findByBudgetId(Integer budgetId) {
        return materialMapper.findByBudgetId(budgetId);
    }

}
