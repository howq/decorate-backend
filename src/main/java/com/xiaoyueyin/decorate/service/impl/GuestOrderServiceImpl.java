package com.xiaoyueyin.decorate.service.impl;

import com.xiaoyueyin.decorate.core.AbstractService;
import com.xiaoyueyin.decorate.dao.GuestOrderMapper;
import com.xiaoyueyin.decorate.model.GuestOrder;
import com.xiaoyueyin.decorate.model.GuestOrderQuery;
import com.xiaoyueyin.decorate.service.GuestOrderService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;


/**
 * Created by CodeGenerator on 2018/05/11.
 */
@Service
@Transactional
public class GuestOrderServiceImpl extends AbstractService<GuestOrder> implements GuestOrderService {
    @Resource
    private GuestOrderMapper guestOrderMapper;

    @Override
    public List<GuestOrder> orderInfo(GuestOrderQuery guestOrderQuery) {
        return guestOrderMapper.orderInfo(guestOrderQuery);
    }
}
