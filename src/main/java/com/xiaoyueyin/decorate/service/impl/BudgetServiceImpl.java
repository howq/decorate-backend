package com.xiaoyueyin.decorate.service.impl;

import com.xiaoyueyin.decorate.dao.BudgetMapper;
import com.xiaoyueyin.decorate.model.Budget;
import com.xiaoyueyin.decorate.service.BudgetService;
import com.xiaoyueyin.decorate.core.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by CodeGenerator on 2018/04/30.
 */
@Service
@Transactional
public class BudgetServiceImpl extends AbstractService<Budget> implements BudgetService {
    @Resource
    private BudgetMapper budgetMapper;

}
