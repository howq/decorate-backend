package com.xiaoyueyin.decorate.service.impl;

import com.xiaoyueyin.decorate.dao.DesignMapper;
import com.xiaoyueyin.decorate.model.Design;
import com.xiaoyueyin.decorate.service.DesignService;
import com.xiaoyueyin.decorate.core.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by CodeGenerator on 2018/04/30.
 */
@Service
@Transactional
public class DesignServiceImpl extends AbstractService<Design> implements DesignService {
    @Resource
    private DesignMapper designMapper;

}
