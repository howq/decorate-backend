package com.xiaoyueyin.decorate.service.impl;

import com.xiaoyueyin.decorate.dao.GuestMapper;
import com.xiaoyueyin.decorate.model.Guest;
import com.xiaoyueyin.decorate.service.GuestService;
import com.xiaoyueyin.decorate.core.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by CodeGenerator on 2018/04/30.
 */
@Service
@Transactional
public class GuestServiceImpl extends AbstractService<Guest> implements GuestService {
    @Resource
    private GuestMapper guestMapper;

}
