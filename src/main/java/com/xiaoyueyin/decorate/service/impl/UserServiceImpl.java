package com.xiaoyueyin.decorate.service.impl;

import com.xiaoyueyin.decorate.dao.UserMapper;
import com.xiaoyueyin.decorate.model.User;
import com.xiaoyueyin.decorate.service.UserService;
import com.xiaoyueyin.decorate.core.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by CodeGenerator on 2018/04/30.
 */
@Service
@Transactional
public class UserServiceImpl extends AbstractService<User> implements UserService {
    @Resource
    private UserMapper userMapper;

    @Override
    public User checkPwd(String username, String pwd) throws Exception {
        User tmp = new User();
        tmp.setUsername(username);
        User user = userMapper.selectOne(tmp);
        String password = user.getPassword();
        if (pwd.equals(password)) {
            logger.info("校验用户密码成功！");
            return user;
        }
        return null;
    }

}
