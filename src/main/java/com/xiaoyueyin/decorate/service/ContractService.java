package com.xiaoyueyin.decorate.service;
import com.xiaoyueyin.decorate.model.Contract;
import com.xiaoyueyin.decorate.core.Service;


/**
 * Created by CodeGenerator on 2018/04/30.
 */
public interface ContractService extends Service<Contract> {

}
