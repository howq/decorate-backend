package com.xiaoyueyin.decorate.service.cache.impl;

import com.xiaoyueyin.decorate.service.cache.CacheService;
import com.xiaoyueyin.decorate.service.cache.model.CustLoginSession;
import com.xiaoyueyin.decorate.util.Result;
import com.xiaoyueyin.decorate.util.ValSystemException;
import com.xiaoyueyin.decorate.util.cache.impl.RedisCacheServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.List;

/**
 * 分布式缓存服务。
 *
 * @author xiaoyueyin
 */
@Service("cacheService")
public class CacheServiceImpl implements CacheService {

    private static final Logger logger = LoggerFactory.getLogger(CacheServiceImpl.class);

    @Resource(name = "redisCacheServiceImpl")
    private RedisCacheServiceImpl redisCacheServiceImpl;

    @Override
    public void setCustLoginSession(CustLoginSession session) {
        if (session != null) {
            logger.debug("Put login session to cahce. token:" + session.getToken());

            int custLoginSessionExpireTime = 60*60*30;

            Result<String> result = redisCacheServiceImpl.saveByObject(session.getToken(), session, custLoginSessionExpireTime);
            if (!result.isSuccess()) {
                logger.warn("Failed to save login session to cache.");
                throw new ValSystemException("Failed to save login session to cache.");
            }
            logger.debug("Put login session to redis success");
        }
    }

    @Override
    public CustLoginSession getCustLoginSession(String token) {
        if (token != null) {

            logger.debug("Get login session from cache. token:" + token);

            CustLoginSession loginSession = null;
            loginSession = (CustLoginSession) redisCacheServiceImpl.getByClass(token, CustLoginSession.class);

            return loginSession;
        }

        return null;
    }

    @Override
    public void removeCustLoginSession(String token) {
        if (token != null) {

            logger.debug("Remove login session from cahce. token:" + token);

            redisCacheServiceImpl.delByKey(token);
        }
    }

    @Override
    public boolean saveUserToken(String username, List<String> tokenList) {
        if (!StringUtils.isEmpty(username)) {
            logger.debug("Put username to cahce. username:" + username);

            Result<String> result = redisCacheServiceImpl.saveByObject(username, tokenList);
            if (!result.isSuccess()) {
                logger.warn("Failed to save username to cache.");
                throw new RuntimeException("Failed to save username to cache.");
            }
            logger.debug("Put username to redis success");
            return result.isSuccess();
        }
        return false;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> getUserToken(String username) {
        if (!StringUtils.isEmpty(username)) {
            return (List<String>) redisCacheServiceImpl.getByClass(username, List.class);
        }
        return null;
    }
}
