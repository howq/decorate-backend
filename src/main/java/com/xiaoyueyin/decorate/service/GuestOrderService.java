package com.xiaoyueyin.decorate.service;
import com.xiaoyueyin.decorate.model.GuestOrder;
import com.xiaoyueyin.decorate.core.Service;
import com.xiaoyueyin.decorate.model.GuestOrderQuery;

import java.util.List;


/**
 * Created by CodeGenerator on 2018/05/11.
 */
public interface GuestOrderService extends Service<GuestOrder> {

    public List<GuestOrder> orderInfo(GuestOrderQuery guestOrderQuery);
}
