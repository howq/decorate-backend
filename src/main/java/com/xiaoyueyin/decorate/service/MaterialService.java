package com.xiaoyueyin.decorate.service;
import com.xiaoyueyin.decorate.model.Material;
import com.xiaoyueyin.decorate.core.Service;

import java.util.List;


/**
 * Created by CodeGenerator on 2018/04/30.
 */
public interface MaterialService extends Service<Material> {

    /**
     *
     * @param budgetId
     * @return
     */
    public List<Material> findByBudgetId(Integer budgetId);

}
