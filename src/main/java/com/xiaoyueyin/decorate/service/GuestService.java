package com.xiaoyueyin.decorate.service;
import com.xiaoyueyin.decorate.model.Guest;
import com.xiaoyueyin.decorate.core.Service;


/**
 * Created by CodeGenerator on 2018/04/30.
 */
public interface GuestService extends Service<Guest> {

}
