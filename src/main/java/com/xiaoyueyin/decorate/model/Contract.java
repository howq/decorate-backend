package com.xiaoyueyin.decorate.model;

import javax.persistence.*;

public class Contract {
    /**
     * 自增唯一ID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 订单ID
     */
    @Column(name = "order_id")
    private Integer orderId;

    /**
     * 说明
     */
    private String introduction;

    /**
     * 合同
     */
    private String pic;

    /**
     * 获取自增唯一ID
     *
     * @return id - 自增唯一ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置自增唯一ID
     *
     * @param id 自增唯一ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取订单ID
     *
     * @return order_id - 订单ID
     */
    public Integer getOrderId() {
        return orderId;
    }

    /**
     * 设置订单ID
     *
     * @param orderId 订单ID
     */
    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    /**
     * 获取说明
     *
     * @return introduction - 说明
     */
    public String getIntroduction() {
        return introduction;
    }

    /**
     * 设置说明
     *
     * @param introduction 说明
     */
    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    /**
     * 获取合同
     *
     * @return pic - 合同
     */
    public String getPic() {
        return pic;
    }

    /**
     * 设置合同
     *
     * @param pic 合同
     */
    public void setPic(String pic) {
        this.pic = pic;
    }
}