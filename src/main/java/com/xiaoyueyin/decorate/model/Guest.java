package com.xiaoyueyin.decorate.model;

import javax.persistence.*;

public class Guest {
    /**
     * 自增唯一ID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 名字
     */
    private String username;

    /**
     * 住址
     */
    private String addr;

    /**
     * 手机号
     */
    private String tel;

    /**
     * 获取自增唯一ID
     *
     * @return id - 自增唯一ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置自增唯一ID
     *
     * @param id 自增唯一ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取名字
     *
     * @return username - 名字
     */
    public String getUsername() {
        return username;
    }

    /**
     * 设置名字
     *
     * @param username 名字
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * 获取住址
     *
     * @return addr - 住址
     */
    public String getAddr() {
        return addr;
    }

    /**
     * 设置住址
     *
     * @param addr 住址
     */
    public void setAddr(String addr) {
        this.addr = addr;
    }

    /**
     * 获取手机号
     *
     * @return tel - 手机号
     */
    public String getTel() {
        return tel;
    }

    /**
     * 设置手机号
     *
     * @param tel 手机号
     */
    public void setTel(String tel) {
        this.tel = tel;
    }
}