package com.xiaoyueyin.decorate.model;

import javax.persistence.*;

public class User {
    /**
     * 自增唯一ID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 名字
     */
    private String username;

    /**
     * 性别
     */
    private String sex;

    /**
     * 职务：1管理员,2跟单员,3设计师
     */
    private String duty;

    /**
     * 密码
     */
    private String password;

    /**
     * 住址
     */
    private String addr;

    /**
     * 手机号
     */
    private String tel;

    /**
     * 获取自增唯一ID
     *
     * @return id - 自增唯一ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置自增唯一ID
     *
     * @param id 自增唯一ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取名字
     *
     * @return username - 名字
     */
    public String getUsername() {
        return username;
    }

    /**
     * 设置名字
     *
     * @param username 名字
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * 获取性别
     *
     * @return sex - 性别
     */
    public String getSex() {
        return sex;
    }

    /**
     * 设置性别
     *
     * @param sex 性别
     */
    public void setSex(String sex) {
        this.sex = sex;
    }

    /**
     * 获取职务：1管理员,2跟单员,3设计师
     *
     * @return duty - 职务：1管理员,2跟单员,3设计师
     */
    public String getDuty() {
        return duty;
    }

    /**
     * 设置职务：1管理员,2跟单员,3设计师
     *
     * @param duty 职务：1管理员,2跟单员,3设计师
     */
    public void setDuty(String duty) {
        this.duty = duty;
    }

    /**
     * 获取密码
     *
     * @return password - 密码
     */
    public String getPassword() {
        return password;
    }

    /**
     * 设置密码
     *
     * @param password 密码
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * 获取住址
     *
     * @return addr - 住址
     */
    public String getAddr() {
        return addr;
    }

    /**
     * 设置住址
     *
     * @param addr 住址
     */
    public void setAddr(String addr) {
        this.addr = addr;
    }

    /**
     * 获取手机号
     *
     * @return tel - 手机号
     */
    public String getTel() {
        return tel;
    }

    /**
     * 设置手机号
     *
     * @param tel 手机号
     */
    public void setTel(String tel) {
        this.tel = tel;
    }
}