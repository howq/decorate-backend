package com.xiaoyueyin.decorate.model;

import javax.persistence.*;

public class Design {
    /**
     * 自增唯一ID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 订单ID
     */
    @Column(name = "order_id")
    private Integer orderId;

    /**
     * 设计师编号
     */
    @Column(name = "designer_id")
    private Integer designerId;

    /**
     * 说明
     */
    private String introduction;

    /**
     * 附图
     */
    private String pic;

    /**
     * 获取自增唯一ID
     *
     * @return id - 自增唯一ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置自增唯一ID
     *
     * @param id 自增唯一ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取订单ID
     *
     * @return order_id - 订单ID
     */
    public Integer getOrderId() {
        return orderId;
    }

    /**
     * 设置订单ID
     *
     * @param orderId 订单ID
     */
    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    /**
     * 获取设计师编号
     *
     * @return designer_id - 设计师编号
     */
    public Integer getDesignerId() {
        return designerId;
    }

    /**
     * 设置设计师编号
     *
     * @param designerId 设计师编号
     */
    public void setDesignerId(Integer designerId) {
        this.designerId = designerId;
    }

    /**
     * 获取说明
     *
     * @return introduction - 说明
     */
    public String getIntroduction() {
        return introduction;
    }

    /**
     * 设置说明
     *
     * @param introduction 说明
     */
    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    /**
     * 获取附图
     *
     * @return pic - 附图
     */
    public String getPic() {
        return pic;
    }

    /**
     * 设置附图
     *
     * @param pic 附图
     */
    public void setPic(String pic) {
        this.pic = pic;
    }
}