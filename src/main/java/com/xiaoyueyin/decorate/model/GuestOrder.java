package com.xiaoyueyin.decorate.model;

import javax.persistence.*;

@Table(name = "guest_order")
public class GuestOrder {
    /**
     * 自增唯一ID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 客户ID
     */
    @Column(name = "guest_id")
    private Integer guestId;

    /**
     * 设计师编号
     */
    @Column(name = "designer_id")
    private Integer designerId;

    /**
     * 住址
     */
    private String addr;

    /**
     * 面积
     */
    private Double area;

    /**
     * 室
     */
    private Integer shi;

    /**
     * 厅
     */
    private Integer ting;

    /**
     * 卫
     */
    private Integer wei;

    /**
     * 附图
     */
    private String pic;

    /**
     * 步骤 0:设计中，1：预算中，2：合同撰写中，3：施工中
     */
    private String step;

    /**
     * 步骤 0:设计中，1：预算中，2：合同撰写中，3：施工中
     */
    @Transient
    private String stepZh;

    /**
     * 设计图id

     */
    @Transient
    private Integer designId;

     /**
     * 设计图介绍
     */
    @Transient
    private String designIntroduction;

    /**
     * 设计图附图
     */
    @Transient
    private String designPic;

    /**
     * 预算id
     */
    @Transient
    private Integer budgetId;

    /**
     * 合同id

     */
    @Transient
    private Integer contractId;

    /**
     * 合同介绍
     */
    @Transient
    private String contractIntroduction;

    /**
     * 合同附图
     */
    @Transient
    private String contractPic;

    /**
     * 获取自增唯一ID
     *
     * @return id - 自增唯一ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置自增唯一ID
     *
     * @param id 自增唯一ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取客户ID
     *
     * @return guest_id - 客户ID
     */
    public Integer getGuestId() {
        return guestId;
    }

    /**
     * 设置客户ID
     *
     * @param guestId 客户ID
     */
    public void setGuestId(Integer guestId) {
        this.guestId = guestId;
    }

    /**
     * 获取设计师编号
     *
     * @return designer_id - 设计师编号
     */
    public Integer getDesignerId() {
        return designerId;
    }

    /**
     * 设置设计师编号
     *
     * @param designerId 设计师编号
     */
    public void setDesignerId(Integer designerId) {
        this.designerId = designerId;
    }

    /**
     * 获取住址
     *
     * @return addr - 住址
     */
    public String getAddr() {
        return addr;
    }

    /**
     * 设置住址
     *
     * @param addr 住址
     */
    public void setAddr(String addr) {
        this.addr = addr;
    }

    /**
     * 获取面积
     *
     * @return area - 面积
     */
    public Double getArea() {
        return area;
    }

    /**
     * 设置面积
     *
     * @param area 面积
     */
    public void setArea(Double area) {
        this.area = area;
    }

    /**
     * 获取室
     *
     * @return shi - 室
     */
    public Integer getShi() {
        return shi;
    }

    /**
     * 设置室
     *
     * @param shi 室
     */
    public void setShi(Integer shi) {
        this.shi = shi;
    }

    /**
     * 获取厅
     *
     * @return ting - 厅
     */
    public Integer getTing() {
        return ting;
    }

    /**
     * 设置厅
     *
     * @param ting 厅
     */
    public void setTing(Integer ting) {
        this.ting = ting;
    }

    /**
     * 获取卫
     *
     * @return wei - 卫
     */
    public Integer getWei() {
        return wei;
    }

    /**
     * 设置卫
     *
     * @param wei 卫
     */
    public void setWei(Integer wei) {
        this.wei = wei;
    }

    /**
     * 获取附图
     *
     * @return pic - 附图
     */
    public String getPic() {
        return pic;
    }

    /**
     * 设置附图
     *
     * @param pic 附图
     */
    public void setPic(String pic) {
        this.pic = pic;
    }

    /**
     * 获取设计图id

     *
     * @return design_id - 设计图id

     */
    public Integer getDesignId() {
        return designId;
    }

    /**
     * 设置设计图id

     *
     * @param designId 设计图id

     */
    public void setDesignId(Integer designId) {
        this.designId = designId;
    }

    /**
     * 获取预算id

     *
     * @return budget_id - 预算id

     */
    public Integer getBudgetId() {
        return budgetId;
    }

    /**
     * 设置预算id

     *
     * @param budgetId 预算id

     */
    public void setBudgetId(Integer budgetId) {
        this.budgetId = budgetId;
    }

    /**
     * 获取合同id

     *
     * @return contract_id - 合同id

     */
    public Integer getContractId() {
        return contractId;
    }

    /**
     * 设置合同id

     *
     * @param contractId 合同id

     */
    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    /**
     * Getter for property 'designIntroduction'.
     *
     * @return Value for property 'designIntroduction'.
     */
    public String getDesignIntroduction() {
        return designIntroduction;
    }

    /**
     * Setter for property 'designIntroduction'.
     *
     * @param designIntroduction Value to set for property 'designIntroduction'.
     */
    public void setDesignIntroduction(String designIntroduction) {
        this.designIntroduction = designIntroduction;
    }

    /**
     * Getter for property 'designPic'.
     *
     * @return Value for property 'designPic'.
     */
    public String getDesignPic() {
        return designPic;
    }

    /**
     * Setter for property 'designPic'.
     *
     * @param designPic Value to set for property 'designPic'.
     */
    public void setDesignPic(String designPic) {
        this.designPic = designPic;
    }

    /**
     * Getter for property 'contractIntroduction'.
     *
     * @return Value for property 'contractIntroduction'.
     */
    public String getContractIntroduction() {
        return contractIntroduction;
    }

    /**
     * Setter for property 'contractIntroduction'.
     *
     * @param contractIntroduction Value to set for property 'contractIntroduction'.
     */
    public void setContractIntroduction(String contractIntroduction) {
        this.contractIntroduction = contractIntroduction;
    }

    /**
     * Getter for property 'contractPic'.
     *
     * @return Value for property 'contractPic'.
     */
    public String getContractPic() {
        return contractPic;
    }

    /**
     * Setter for property 'contractPic'.
     *
     * @param contractPic Value to set for property 'contractPic'.
     */
    public void setContractPic(String contractPic) {
        this.contractPic = contractPic;
    }

    /**
     * 步骤 0:设计中，1：预算中，2：合同撰写中，3：施工中
     *
     * Getter for property 'step'.
     *
     * @return Value for property 'step'.
     */
    public String getStep() {
        return this.step;
    }

    /**
     * 步骤 0:设计中，1：预算中，2：合同撰写中，3：施工中
     *
     * Setter for property 'step'.
     *
     * @param step Value to set for property 'step'.
     */
    public void setStep(String step) {
        this.step = step;
    }

    /**
     * Getter for property 'stepZh'.
     *
     * @return Value for property 'stepZh'.
     */
    public String getStepZh() {
        String result;
        switch (this.step) {
            case "0": result= "设计中";break;
            case "1": result= "预算中";break;
            case "2": result= "合同撰写中";break;
            case "3": result= "施工中";break;
            default:
                return step;
        }
        return result;
    }

    /**
     * Setter for property 'stepZh'.
     *
     * @param stepZh Value to set for property 'stepZh'.
     */
    public void setStepZh(String stepZh) {
        this.stepZh = stepZh;
    }
}