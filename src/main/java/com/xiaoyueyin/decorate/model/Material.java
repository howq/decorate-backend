package com.xiaoyueyin.decorate.model;

import javax.persistence.*;

public class Material {
    /**
     * 自增唯一ID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 预算ID
     */
    @Column(name = "budget_id")
    private Integer budgetId;

    /**
     * 建材名称
     */
    private String name;

    /**
     * 价格
     */
    private double price;

    /**
     * 单位
     */
    private String unit;

    /**
     * 数量
     */
    private int quantity;

    /**
     * 总价
     */
    @Transient
    private double totalPrice;

    /**
     * 获取自增唯一ID
     *
     * @return id - 自增唯一ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置自增唯一ID
     *
     * @param id 自增唯一ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取预算ID
     *
     * @return budget_id - 预算ID
     */
    public Integer getBudgetId() {
        return budgetId;
    }

    /**
     * 设置预算ID
     *
     * @param budgetId 预算ID
     */
    public void setBudgetId(Integer budgetId) {
        this.budgetId = budgetId;
    }

    /**
     * 获取建材名称
     *
     * @return name - 建材名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置建材名称
     *
     * @param name 建材名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取价格
     *
     * @return price - 价格
     */
    public double getPrice() {
        return price;
    }

    /**
     * 设置价格
     *
     * @param price 价格
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * 获取单位
     *
     * @return unit - 单位
     */
    public String getUnit() {
        return unit;
    }

    /**
     * 设置单位
     *
     * @param unit 单位
     */
    public void setUnit(String unit) {
        this.unit = unit;
    }

    /**
     * 获取数量
     *
     * @return quantity - 数量
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * 设置数量
     *
     * @param quantity 数量
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * Getter for property 'totalPrice'.
     *
     * @return Value for property 'totalPrice'.
     */
    public double getTotalPrice() {
        return price * quantity;
    }

    /**
     * Setter for property 'totalPrice'.
     *
     * @param totalPrice Value to set for property 'totalPrice'.
     */
    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }
}