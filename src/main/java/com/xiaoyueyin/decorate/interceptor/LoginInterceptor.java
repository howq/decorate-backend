package com.xiaoyueyin.decorate.interceptor;

import com.alibaba.fastjson.JSON;
import com.xiaoyueyin.decorate.core.Result;
import com.xiaoyueyin.decorate.core.ResultCode;
import com.xiaoyueyin.decorate.service.cache.SessionService;
import com.xiaoyueyin.decorate.service.cache.model.CustLoginSession;
import com.xiaoyueyin.decorate.util.IPUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 登陆拦截器
 *
 * @author xiaoyueyin
 * @create 2017/3/9 14:17
 **/
public class LoginInterceptor implements HandlerInterceptor {

    private static final Logger logger = LoggerFactory.getLogger(LoginInterceptor.class);

    @Resource
    private SessionService sessionService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        logger.debug("request path：" + request.getRequestURI());
        try {
            CustLoginSession loginSession = sessionService.getSession(request);
            String uri = request.getRequestURI();
            logger.debug("request path：" + uri);
            if (loginSession == null) {
                logger.warn("签名认证失败，请求接口：{}，请求IP：{}，请求参数：{}",
                        request.getRequestURI(), IPUtil.getIpAddr(request), JSON.toJSONString(request.getParameterMap()));

                Result result = new Result();
                result.setCode(ResultCode.UNAUTHORIZED).setMessage("签名认证失败");
                responseResult(response, result);
                return false;
            }
        } catch (Exception e) {
            logger.warn("签名认证失败，请求接口：{}，请求IP：{}，请求参数：{}",
                    request.getRequestURI(), IPUtil.getIpAddr(request), JSON.toJSONString(request.getParameterMap()));
            Result result = new Result();
            result.setCode(ResultCode.UNAUTHORIZED).setMessage("签名认证失败");
            responseResult(response, result);
            return false;
        }
        return true;
    }

    private void responseResult(HttpServletResponse response, Result result) {
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Content-type", "application/json;charset=UTF-8");
        response.setStatus(200);
        try {
            response.getWriter().write(JSON.toJSONString(result));
        } catch (IOException ex) {
            logger.error(ex.getMessage());
        }
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object o, Exception arg3) throws Exception {


    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse arg1, Object arg2, ModelAndView arg3) throws Exception {

    }
}
