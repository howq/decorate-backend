package com.xiaoyueyin.decorate.dao;

import com.xiaoyueyin.decorate.core.Mapper;
import com.xiaoyueyin.decorate.model.Guest;

public interface GuestMapper extends Mapper<Guest> {
}