package com.xiaoyueyin.decorate.dao;

import com.xiaoyueyin.decorate.core.Mapper;
import com.xiaoyueyin.decorate.model.Budget;

public interface BudgetMapper extends Mapper<Budget> {
}