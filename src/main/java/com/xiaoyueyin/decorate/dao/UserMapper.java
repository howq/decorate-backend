package com.xiaoyueyin.decorate.dao;

import com.xiaoyueyin.decorate.core.Mapper;
import com.xiaoyueyin.decorate.model.User;

public interface UserMapper extends Mapper<User> {
}