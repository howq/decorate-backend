package com.xiaoyueyin.decorate.dao;

import com.xiaoyueyin.decorate.core.Mapper;
import com.xiaoyueyin.decorate.model.Material;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MaterialMapper extends Mapper<Material> {

    /**
     *
     * @param budgetId
     * @return
     */
    public List<Material> findByBudgetId(@Param(value = "budgetId") Integer budgetId);
}