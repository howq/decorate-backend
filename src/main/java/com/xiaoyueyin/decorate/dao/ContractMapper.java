package com.xiaoyueyin.decorate.dao;

import com.xiaoyueyin.decorate.core.Mapper;
import com.xiaoyueyin.decorate.model.Contract;

public interface ContractMapper extends Mapper<Contract> {
}