package com.xiaoyueyin.decorate.dao;

import com.xiaoyueyin.decorate.core.Mapper;
import com.xiaoyueyin.decorate.model.Design;

public interface DesignMapper extends Mapper<Design> {
}