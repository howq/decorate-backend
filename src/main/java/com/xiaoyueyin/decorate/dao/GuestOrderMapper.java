package com.xiaoyueyin.decorate.dao;

import com.xiaoyueyin.decorate.core.Mapper;
import com.xiaoyueyin.decorate.model.GuestOrder;
import com.xiaoyueyin.decorate.model.GuestOrderQuery;

import java.util.List;

public interface GuestOrderMapper extends Mapper<GuestOrder> {

    List<GuestOrder> orderInfo(GuestOrderQuery guestOrderQuery);
}