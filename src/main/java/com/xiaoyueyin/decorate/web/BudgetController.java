package com.xiaoyueyin.decorate.web;

import com.xiaoyueyin.decorate.core.Result;
import com.xiaoyueyin.decorate.core.ResultGenerator;
import com.xiaoyueyin.decorate.model.Budget;
import com.xiaoyueyin.decorate.model.GuestOrder;
import com.xiaoyueyin.decorate.service.BudgetService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xiaoyueyin.decorate.service.GuestOrderService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
* Created by CodeGenerator on 2018/04/30.
*/
@RestController
@RequestMapping("/budget")
public class BudgetController {
    @Resource
    private BudgetService budgetService;

    @Resource
    private GuestOrderService guestOrderService;

    @PostMapping
    public Result add(@RequestBody Budget budget) {
        budgetService.save(budget);

        GuestOrder guestOrder = guestOrderService.findById(budget.getOrderId());
        guestOrder.setStep("2");
        guestOrderService.update(guestOrder);

        return ResultGenerator.genSuccessResult();
    }

    @DeleteMapping("/{id}")
    public Result delete(@PathVariable Integer id) {
        budgetService.deleteById(id);
        return ResultGenerator.genSuccessResult();
    }

    @PutMapping
    public Result update(@RequestBody Budget budget) {
        budgetService.update(budget);
        return ResultGenerator.genSuccessResult();
    }

    @GetMapping("/{id}")
    public Result detail(@PathVariable Integer id) {
        Budget budget = budgetService.findById(id);
        return ResultGenerator.genSuccessResult(budget);
    }

    @GetMapping
    public Result list(@RequestParam(defaultValue = "0") Integer page, @RequestParam(defaultValue = "0") Integer size) {
        PageHelper.startPage(page, size);
        List<Budget> list = budgetService.findAll();
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }
}
