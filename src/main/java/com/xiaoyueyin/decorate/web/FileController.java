package com.xiaoyueyin.decorate.web;

import com.xiaoyueyin.decorate.core.Result;
import com.xiaoyueyin.decorate.core.ResultGenerator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;

/**
 * @author xiaoyueyin
 * @create 2018-05-02 下午7:07
 **/
@RestController
@RequestMapping("/file")
public class FileController {

    /**
     * 文件路径
     */
    @Value("${decorate.file.path}")
    private String path;

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public Result upload(HttpServletRequest request,
                         @RequestParam("file") MultipartFile file) throws Exception {

        //如果文件不为空，写入上传路径
        if (!file.isEmpty()) {
            //上传文件名
            String filename = file.getOriginalFilename();
            File filepath = new File(path, filename);
            //判断路径是否存在，如果不存在就创建一个
            if (!filepath.getParentFile().exists()) {
                filepath.getParentFile().mkdirs();
            }
            //将上传文件保存到一个目标文件当中
            file.transferTo(new File(path + File.separator + filename));
            return ResultGenerator.genSuccessResult(filename);
        } else {
             return ResultGenerator.genFailResult("file is empty");
        }

    }
}