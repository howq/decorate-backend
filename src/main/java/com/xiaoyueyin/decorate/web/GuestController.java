package com.xiaoyueyin.decorate.web;

import com.xiaoyueyin.decorate.core.Result;
import com.xiaoyueyin.decorate.core.ResultGenerator;
import com.xiaoyueyin.decorate.model.Guest;
import com.xiaoyueyin.decorate.service.GuestService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by CodeGenerator on 2018/04/30.
 */
@RestController
@RequestMapping("/guest")
public class GuestController {
    @Resource
    private GuestService guestService;

    @PostMapping
    public Result add(@RequestBody Guest guest) {
        guestService.save(guest);
        return ResultGenerator.genSuccessResult();
    }

    @DeleteMapping("/{id}")
    public Result delete(@PathVariable Integer id) {
        guestService.deleteById(id);
        return ResultGenerator.genSuccessResult();
    }

    @PutMapping
    public Result update(@RequestBody Guest guest) {
        guestService.update(guest);
        return ResultGenerator.genSuccessResult();
    }

    @GetMapping("/{id}")
    public Result detail(@PathVariable Integer id) {
        Guest guest = guestService.findById(id);
        return ResultGenerator.genSuccessResult(guest);
    }

    @GetMapping
    public Result list(@RequestParam(defaultValue = "0") Integer page, @RequestParam(defaultValue = "0") Integer size) {

//        Condition condition = new Condition(Guest.class);
//        condition.createCriteria().orLike("username", guest.getUsername());
//        PageInfo pageInfo = PageHelper.startPage(page, size).doSelectPageInfo(()-> guestService.findByCondition(condition));

        PageHelper.startPage(page, size);
        List<Guest> list = guestService.findAll();
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }
}
