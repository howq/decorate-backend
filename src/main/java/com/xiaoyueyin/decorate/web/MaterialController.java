package com.xiaoyueyin.decorate.web;

import com.xiaoyueyin.decorate.core.Result;
import com.xiaoyueyin.decorate.core.ResultGenerator;
import com.xiaoyueyin.decorate.model.GuestOrder;
import com.xiaoyueyin.decorate.model.Material;
import com.xiaoyueyin.decorate.service.GuestOrderService;
import com.xiaoyueyin.decorate.service.MaterialService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
* Created by CodeGenerator on 2018/04/30.
*/
@RestController
@RequestMapping("/material")
public class MaterialController {
    @Resource
    private MaterialService materialService;


    @Resource
    private GuestOrderService guestOrderService;

    @PostMapping
    public Result add(@RequestBody Material material) {
        materialService.save(material);

        GuestOrder guestOrder = guestOrderService.findById(material.getBudgetId());
        guestOrder.setStep("2");
        guestOrderService.update(guestOrder);

        return ResultGenerator.genSuccessResult();
    }

    @DeleteMapping("/{id}")
    public Result delete(@PathVariable Integer id) {
        materialService.deleteById(id);
        return ResultGenerator.genSuccessResult();
    }

    @PutMapping
    public Result update(@RequestBody Material material) {
        materialService.update(material);
        return ResultGenerator.genSuccessResult();
    }

    @GetMapping("/{id}")
    public Result detail(@PathVariable Integer id) {
        Material material = materialService.findById(id);
        return ResultGenerator.genSuccessResult(material);
    }

    @GetMapping
    public Result list(@RequestParam(defaultValue = "0") Integer page, @RequestParam(defaultValue = "0") Integer size, @RequestParam(value = "budgetId", required = false) Integer budgetId) {
        PageHelper.startPage(page, size);
        List<Material> list = materialService.findByBudgetId(budgetId);
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }
}
