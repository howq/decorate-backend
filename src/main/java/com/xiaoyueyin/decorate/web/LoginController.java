package com.xiaoyueyin.decorate.web;


import com.xiaoyueyin.decorate.model.User;
import com.xiaoyueyin.decorate.service.UserService;
import com.xiaoyueyin.decorate.service.cache.SessionService;
import com.xiaoyueyin.decorate.service.cache.model.CustLoginSession;
import com.xiaoyueyin.decorate.util.IPUtil;
import com.xiaoyueyin.decorate.util.Result;
import com.xiaoyueyin.decorate.util.ValidateUtil;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

/**
 * 登陆入口
 *
 * @author xiaoyueyin
 * @create 2017 /3/9 14:17
 */
@RestController
@RequestMapping("/login")
public class LoginController extends WebExceptionHandler {

    @Resource
    private UserService userService;

    @Resource(name = "sessionService")
    private SessionService sessionService;


    /**
     * Login result.
     *
     * @param request    the request
     * @param response   the response
     * @return the result
     */
    @RequestMapping(value = "/doLogin", method = RequestMethod.POST)
    public Result<Object> login(HttpServletRequest request, HttpServletResponse response, @RequestBody User user1) {
        logger.info("=======================进行登录操作=======================");
        Result<Object> result = new Result<Object>();

        String username = user1.getUsername();
        String password = user1.getPassword();

        // 输入项目检查
//        if (!chkInputInfo(username, password)) {
//            result.setCode(Result.Code.ERROR);
//            result.setMessage("登陆失败");
//            logger.info("登录输入格式错误");
//            return result;
//        }
        try {
            User user = userService.checkPwd(username, password);
            if (null != user) {
                logger.info("=======================获取session开始=======================");
                HttpSession session = request.getSession();
                logger.info("=======================获取session结束=======================");
                StringBuilder url = new StringBuilder();

                CustLoginSession loginSession = new CustLoginSession();
                Map<String, Object> resultMap = new HashMap<String, Object>();
                resultMap.put("url", url.toString());
                logger.info("用户:[" + user.getUsername() + "] 请求的ip地址：[" + IPUtil.getIpAddr(request) + "] 使用的浏览器版本：[" + request.getHeader("USER-AGENT") + "]");
                session.setAttribute("user", user);
                // 保存用户信息到session中
                loginSession.setUser(user);
                loginSession.setSessionId(session.getId());
                logger.info("=======================创建用户session信息========================");
                sessionService.createOrUpdateLoginSession(request, response, loginSession, "1");
                logger.info("=========创建用户session信息结束=========" + username + ":" + "登录成功");
                result.setData(resultMap);
                result.setCode(Result.Code.SUCCESS);
                result.setMessage("登陆成功");
                return result;
            } else {
                logger.info("登陆失败，密码或账号错误----->密码错误");
                result.setCode(Result.Code.ERROR);
                result.setMessage("登陆失败");
                return result;
            }
        } catch (Exception e) {
            logger.error("登陆失败，密码或账号错误----->账号未注册" + e.getMessage());
            result.setCode(Result.Code.ERROR);
            result.setMessage("登陆失败");
            return result;
        }
    }

    /**
     * 其它页面返回Login页面处理
     *
     * @param request  the request
     * @param response the response
     * @return INPUT :成功
     */
    @RequestMapping(value = "/doLogout", method = RequestMethod.GET)
    public Result<Object> logout(HttpServletRequest request, HttpServletResponse response) {
        Result<Object> result = new Result<Object>();
        try {
            HttpSession session = request.getSession();
            session.removeAttribute("user");
            sessionService.removeSession(request, response);
        } catch (Exception e) {
            logger.error("返回登录界面出错", e.getMessage());
        }
        result.setCode(Result.Code.SUCCESS);
        result.setMessage("退出成功");
        return result;
    }

    /**
     * 其它页面返回Login页面处理
     *
     * @param request  the request
     * @param response the response
     * @return INPUT :成功
     */
    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public Result<Object> user(HttpServletRequest request, HttpServletResponse response) {
        Result<Object> result = new Result<Object>();
        try {
            CustLoginSession loginSession = sessionService.getSession(request);
            result.setData(loginSession.getUser());
        } catch (Exception e) {

        }
        result.setCode(Result.Code.SUCCESS);
        result.setMessage("退出成功");
        return result;
    }


    /**
     * 输入项目检查
     *
     * @return true：合法、false：非法
     * @throws Exception
     */
    private boolean chkInputInfo(String username, String password) {
        // 用户登录名为空检查
        if (StringUtils.isEmpty(username)) {
            return false;
        } else {
            // 用户登录名是半角英、数字检查
            if (ValidateUtil.chkEnNum(username) == false) {
                return false;
            }
            // 用户登录名长度不足检查
            if (ValidateUtil.chkItemLength(username, 5, 18) == false) {
                return false;
            }
            // 用户登录密码为空检查
            if (null == password || "".equals(password.trim())) {
                return false;
            }
//            // 用户登录密码是半角英、数字检查
//            if (ValidateUtil.chkEnNum(password) == false) {
//                return false;
//            }
//            // 用户登录密码长度不足检查
//            if (ValidateUtil.chkItemLength(password, 6, 12) == false) {
//                return false;
//            }
        }
        return true;
    }
}
