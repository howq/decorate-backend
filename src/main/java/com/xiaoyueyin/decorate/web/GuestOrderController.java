package com.xiaoyueyin.decorate.web;

import com.xiaoyueyin.decorate.core.Result;
import com.xiaoyueyin.decorate.core.ResultGenerator;
import com.xiaoyueyin.decorate.model.GuestOrder;
import com.xiaoyueyin.decorate.model.GuestOrderQuery;
import com.xiaoyueyin.decorate.service.GuestOrderService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
* Created by CodeGenerator on 2018/05/11.
*/
@RestController
@RequestMapping("/guest/order")
public class GuestOrderController {
    @Resource
    private GuestOrderService guestOrderService;

    @PostMapping
    public Result add(@RequestBody GuestOrder guestOrder) {
        guestOrderService.save(guestOrder);
        return ResultGenerator.genSuccessResult();
    }

    @DeleteMapping("/{id}")
    public Result delete(@PathVariable Integer id) {
        guestOrderService.deleteById(id);
        return ResultGenerator.genSuccessResult();
    }

    @PutMapping
    public Result update(@RequestBody GuestOrder guestOrder) {
        guestOrderService.update(guestOrder);
        return ResultGenerator.genSuccessResult();
    }

    @GetMapping("/{id}")
    public Result detail(@PathVariable Integer id) {
        GuestOrder guestOrder = guestOrderService.findById(id);
        return ResultGenerator.genSuccessResult(guestOrder);
    }

    @GetMapping
    public Result listInfo(GuestOrderQuery guestOrderQuery) {
        PageHelper.startPage(guestOrderQuery.getPage(), guestOrderQuery.getSize());
        List<GuestOrder> list = guestOrderService.orderInfo(guestOrderQuery);
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }

    @GetMapping("/info")
    public Result list(GuestOrderQuery guestOrderQuery) {
        PageHelper.startPage(guestOrderQuery.getPage(), guestOrderQuery.getSize());
        List<GuestOrder> list = guestOrderService.orderInfo(guestOrderQuery);
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }
}
