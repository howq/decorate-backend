package com.xiaoyueyin.decorate.web;

import com.xiaoyueyin.decorate.core.Result;
import com.xiaoyueyin.decorate.core.ResultGenerator;
import com.xiaoyueyin.decorate.model.User;
import com.xiaoyueyin.decorate.service.UserService;
import com.xiaoyueyin.decorate.service.cache.SessionService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
* Created by CodeGenerator on 2018/04/30.
*/
@RestController
@RequestMapping("/user")
public class UserController extends WebExceptionHandler {
    @Resource
    private UserService userService;

    @Resource(name = "sessionService")
    private SessionService sessionService;

    @PostMapping
    public Result add(@RequestBody User user) {
        userService.save(user);
        return ResultGenerator.genSuccessResult();
    }

    @DeleteMapping("/{id}")
    public Result delete(@PathVariable Integer id) {
        userService.deleteById(id);
        return ResultGenerator.genSuccessResult();
    }

    @PutMapping
    public Result update(@RequestBody User user) {
        userService.update(user);
        return ResultGenerator.genSuccessResult();
    }

    @GetMapping("/{id}")
    public Result detail(@PathVariable Integer id) {
        User user = userService.findById(id);
        return ResultGenerator.genSuccessResult(user);
    }

    @GetMapping
    public Result list(@RequestParam(defaultValue = "0") Integer page, @RequestParam(defaultValue = "0") Integer size) {
        PageHelper.startPage(page, size);
        List<User> list = userService.findAll();
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }
}
