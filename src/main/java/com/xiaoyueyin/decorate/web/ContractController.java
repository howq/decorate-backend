package com.xiaoyueyin.decorate.web;

import com.xiaoyueyin.decorate.core.Result;
import com.xiaoyueyin.decorate.core.ResultGenerator;
import com.xiaoyueyin.decorate.model.Contract;
import com.xiaoyueyin.decorate.model.GuestOrder;
import com.xiaoyueyin.decorate.service.ContractService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xiaoyueyin.decorate.service.GuestOrderService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
* Created by CodeGenerator on 2018/04/30.
*/
@RestController
@RequestMapping("/contract")
public class ContractController {
    @Resource
    private ContractService contractService;

    @Resource
    private GuestOrderService guestOrderService;

    @PostMapping
    public Result add(@RequestBody Contract contract) {
        contractService.save(contract);

        GuestOrder guestOrder = guestOrderService.findById(contract.getOrderId());
        guestOrder.setStep("3");
        guestOrderService.update(guestOrder);

        return ResultGenerator.genSuccessResult();
    }

    @DeleteMapping("/{id}")
    public Result delete(@PathVariable Integer id) {
        contractService.deleteById(id);
        return ResultGenerator.genSuccessResult();
    }

    @PutMapping
    public Result update(@RequestBody Contract contract) {
        contractService.update(contract);
        return ResultGenerator.genSuccessResult();
    }

    @GetMapping("/{id}")
    public Result detail(@PathVariable Integer id) {
        Contract contract = contractService.findById(id);
        return ResultGenerator.genSuccessResult(contract);
    }

    @GetMapping
    public Result list(@RequestParam(defaultValue = "0") Integer page, @RequestParam(defaultValue = "0") Integer size) {
        PageHelper.startPage(page, size);
        List<Contract> list = contractService.findAll();
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }
}
