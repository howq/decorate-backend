package com.xiaoyueyin.decorate.web;

import com.xiaoyueyin.decorate.core.Result;
import com.xiaoyueyin.decorate.core.ResultGenerator;
import com.xiaoyueyin.decorate.model.Design;
import com.xiaoyueyin.decorate.model.GuestOrder;
import com.xiaoyueyin.decorate.service.DesignService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xiaoyueyin.decorate.service.GuestOrderService;
import com.xiaoyueyin.decorate.service.cache.SessionService;
import com.xiaoyueyin.decorate.service.cache.model.CustLoginSession;
import org.springframework.http.HttpRequest;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
* Created by CodeGenerator on 2018/04/30.
*/
@RestController
@RequestMapping("/design")
public class DesignController {
    @Resource
    private DesignService designService;

    @Resource
    private GuestOrderService guestOrderService;

    @Resource(name = "sessionService")
    private SessionService sessionService;

    @PostMapping
    public Result add(@RequestBody Design design, HttpServletRequest request) {

        try {
            CustLoginSession loginSession = sessionService.getSession(request);
            design.setDesignerId(loginSession.getUser().getId());
        } catch (Exception e) {

        }

        designService.save(design);

        GuestOrder guestOrder = guestOrderService.findById(design.getOrderId());
        guestOrder.setStep("1");
        guestOrderService.update(guestOrder);

        return ResultGenerator.genSuccessResult();
    }

    @DeleteMapping("/{id}")
    public Result delete(@PathVariable Integer id) {
        designService.deleteById(id);
        return ResultGenerator.genSuccessResult();
    }

    @PutMapping
    public Result update(@RequestBody Design design) {
        designService.update(design);
        return ResultGenerator.genSuccessResult();
    }

    @GetMapping("/{id}")
    public Result detail(@PathVariable Integer id) {
        Design design = designService.findById(id);
        return ResultGenerator.genSuccessResult(design);
    }

    @GetMapping
    public Result list(@RequestParam(defaultValue = "0") Integer page, @RequestParam(defaultValue = "0") Integer size) {
        PageHelper.startPage(page, size);
        List<Design> list = designService.findAll();
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }
}
