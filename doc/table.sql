-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: db_sync
-- ------------------------------------------------------
-- Server version	5.7.22-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `budget`
--

DROP TABLE IF EXISTS `budget`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `budget` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增唯一ID',
  `order_id` bigint(20) unsigned NOT NULL COMMENT '订单ID',
  `introduction` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL COMMENT '说明',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci COMMENT='预算表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `budget`
--

LOCK TABLES `budget` WRITE;
/*!40000 ALTER TABLE `budget` DISABLE KEYS */;
/*!40000 ALTER TABLE `budget` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contract`
--

DROP TABLE IF EXISTS `contract`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contract` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增唯一ID',
  `order_id` bigint(20) unsigned NOT NULL COMMENT '订单ID',
  `introduction` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL COMMENT '说明',
  `pic` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL COMMENT '合同',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci COMMENT='合同表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contract`
--

LOCK TABLES `contract` WRITE;
/*!40000 ALTER TABLE `contract` DISABLE KEYS */;
/*!40000 ALTER TABLE `contract` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `design`
--

DROP TABLE IF EXISTS `design`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `design` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增唯一ID',
  `order_id` bigint(20) unsigned NOT NULL COMMENT '订单ID',
  `designer_id` bigint(20) unsigned DEFAULT NULL COMMENT '设计师编号',
  `introduction` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL COMMENT '说明',
  `pic` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL COMMENT '附图',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci COMMENT='设计图表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `design`
--

LOCK TABLES `design` WRITE;
/*!40000 ALTER TABLE `design` DISABLE KEYS */;
/*!40000 ALTER TABLE `design` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guest`
--

DROP TABLE IF EXISTS `guest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guest` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增唯一ID',
  `username` varchar(90) COLLATE utf8mb4_unicode_520_ci NOT NULL COMMENT '名字',
  `addr` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL COMMENT '住址',
  `tel` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL COMMENT '手机号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci COMMENT='客户表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guest`
--

LOCK TABLES `guest` WRITE;
/*!40000 ALTER TABLE `guest` DISABLE KEYS */;
INSERT INTO `guest` VALUES (2,'客户333','广发森东方','18265457896'),(5,'客户5','西南科技大学东二','18765457896'),(7,'客户7','西南科技大学东二','18765457896'),(8,'客户89','系可扽散','18765457896'),(11,'客户11','西南科技大学东二','18765457896'),(12,'客户12','西南科技大学东二','18765457896'),(13,'客户13','西南科技大学东二','18765457896'),(14,'客户14','西南科技大学东二','18765457896'),(15,'客户15','西南科技大学东二','18765457896'),(16,'客户16','西南科技大学东二','18765457896'),(17,'客户17','西南科技大学东二','18765457896'),(18,'客户18','西南科技大学东二','18765457896'),(19,'客户19','西南科技大学东二','18765457896'),(20,'客户20','西南科技大学东二','18765457896'),(21,'客户21','西南科技大学东二','18765457896'),(22,'客户22','西南科技大学东二','18765457896'),(33,'往昔','东333','18765457896'),(34,'西南','北三','18645457895');
/*!40000 ALTER TABLE `guest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guest_order`
--

DROP TABLE IF EXISTS `guest_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guest_order` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增唯一ID',
  `guest_id` bigint(20) unsigned NOT NULL COMMENT '客户ID',
  `designer_id` bigint(20) unsigned DEFAULT NULL COMMENT '设计师编号',
  `addr` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL COMMENT '住址',
  `area` double NOT NULL COMMENT '面积',
  `shi` int(2) NOT NULL COMMENT '室',
  `ting` int(2) NOT NULL COMMENT '厅',
  `wei` int(2) NOT NULL COMMENT '卫',
  `pic` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL COMMENT '附图',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci COMMENT='订单表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guest_order`
--

LOCK TABLES `guest_order` WRITE;
/*!40000 ALTER TABLE `guest_order` DISABLE KEYS */;
/*!40000 ALTER TABLE `guest_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `material`
--

DROP TABLE IF EXISTS `material`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `material` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增唯一ID',
  `budget_id` bigint(20) unsigned NOT NULL COMMENT '预算ID',
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL COMMENT '建材名称',
  `price` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL COMMENT '价格',
  `unit` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL COMMENT '单位',
  `quantity` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL COMMENT '数量',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci COMMENT='建材表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `material`
--

LOCK TABLES `material` WRITE;
/*!40000 ALTER TABLE `material` DISABLE KEYS */;
/*!40000 ALTER TABLE `material` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增唯一ID',
  `username` varchar(90) COLLATE utf8mb4_unicode_520_ci NOT NULL COMMENT '名字',
  `sex` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL COMMENT '性别',
  `duty` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL COMMENT '职务：1管理员,2跟单员,3设计师',
  `password` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL COMMENT '密码',
  `addr` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL COMMENT '住址',
  `tel` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL COMMENT '手机号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci COMMENT='用户表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','1','1','123','绵阳西南科技大学','18745621354');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-02  2:34:10